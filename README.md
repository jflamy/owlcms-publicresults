## OWLCMS Results Publisher

This application is normally installed on the cloud, to allow people from the internet to see results from a competition.  

Results are sent from the OWLCMS program running at the competition site to this application, which makes them available to the world.  This protects the competition site from the load that many browsers could create.

To install: click on this button.  You will be prompted to create a (free) account on the Heroku cloud if you do not have one already.  Full instructions can be found at  (coming soon).

[![Deploy to Heroku](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)